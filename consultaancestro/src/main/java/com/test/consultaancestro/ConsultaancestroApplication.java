package com.test.consultaancestro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// TODO: Auto-generated Javadoc
/**
 * The Class ConsultaancestroApplication.
 */
@SpringBootApplication
public class ConsultaancestroApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ConsultaancestroApplication.class, args);
	}
}
