package com.test.consultaancestro.service;

import com.test.consultaancestro.pojo.Nodo;

// TODO: Auto-generated Javadoc
/**
 * The Interface Arbol.
 * Es la interface que abstrae los escenarios para crear y consultar un arbol
 * 
 */
public interface Arbol {

	/**
	 * Cargar arbol.
	 */
	void cargarArbol();
	
	/**
	 * Consultar nodo.
	 *
	 * @param valorHijo the valor hijo
	 * @param nodoPadreBusqueda the nodo padre busqueda
	 * @return the nodo
	 */
	public abstract Nodo consultarNodo(int valorHijo, Nodo nodoPadreBusqueda);
	
	/**
	 * Agregar nodo.
	 *
	 * @param valorBase the valor base
	 * @param nivelBase the nivel base
	 * @param baseNodoIz the base nodo iz
	 * @param baseNodoDe the base nodo de
	 * @param padrebase the padrebase
	 */
	public abstract void agregarNodo(int valorBase, int nivelBase, int baseNodoIz, int baseNodoDe, int padrebase);
	
	/**
	 * Buscar padre comun.
	 *
	 * @param hijos the hijos
	 * @return the integer
	 */
	public Integer buscarPadreComun(Integer[] hijos);
	
	/**
	 * Consultar arbol.
	 *
	 * @param nodos the nodos
	 * @return the nodo
	 */
	public abstract Integer consultarArbol(Integer[] nodos);


}
