package com.test.consultaancestro.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.test.consultaancestro.pojo.Nodo;
import com.test.consultaancestro.service.Arbol;

// TODO: Auto-generated Javadoc
/**
 * The Class ArbolBinario.
 */
@Service("arbolBinario")
public class ArbolBinario implements Arbol{

	/** The nodo raiz. */
	Nodo nodoRaiz;

	/** The nodo anterior. */
	Nodo nodoAnterior;


	/* (non-Javadoc)
	 * @see com.test.consultaancestro.service.Arbol#cargarArbol()
	 * Carga el arbol inicial tomando la informacion de una base de datos en este caso de un arreglo multidimensional
	 */
	@Override
	public void cargarArbol() {
			int[][] matrizArbol = { { 49, 84, 70, 0, 0 }, { 37, 54, 49, 70, 1 }, { 22, 40, 37, 49, 2 }, { 51, 0, 54, 49, 2 }, { 78, 85, 84, 70, 1 }, { 76, 80, 78, 84, 2 }, { 0, 0, 22, 37, 3 }, { 0, 0, 40, 37, 3 }, { 0, 0, 51, 54, 3 }, { 0, 0, 76, 78, 3 }, { 0, 0, 80, 78, 3 }, { 0, 0, 85, 84, 3 } };

			for (int i = 0; i < matrizArbol.length; i++) {
				int baseNodoIz = matrizArbol[i][0];
				int baseNodoDe = matrizArbol[i][1];
				int valorBase = matrizArbol[i][2];
				int padrebase = matrizArbol[i][3];
				int nivelBase = matrizArbol[i][4];

				agregarNodo(valorBase, nivelBase, baseNodoIz, baseNodoDe, padrebase);
			}
//			 imprimirArbol(nodoRaiz);
	}
	
	/**
	 * Imprimir arbol.
	 * Permite imprimir el arbol para analizarlo
	 * @param nodo the nodo
	 */
	private void imprimirArbol(Nodo nodo) {
		if (nodo != null) {
//			System.out.println("Padre: " + nodo.getValor() + " NodoHijo Iz: " + (nodo.getNodoizquierdo() != null ? nodo.getNodoizquierdo().getValor() : "No tiene") + " NodoHijo Der: " + (nodo.getNododerecho() != null ? nodo.getNododerecho().getValor() : "No tiene") + " Padre: "
//					+ (nodo.getNodoPadre() != null ? nodo.getNodoPadre().getValor() : "No tiene"));
			imprimirArbol(nodo.getNodoizquierdo());
			imprimirArbol(nodo.getNododerecho());
		}
	}
	
	/* (non-Javadoc)
	 * Permite agregar un nodo al arbol
	 * @see com.test.consultaancestro.service.Arbol#agregarNodo(int, int, int, int, int)
	 */
	@Override
	public void agregarNodo(int valorBase, int nivelBase, int baseNodoIz, int baseNodoDe, int padrebase) {
		Nodo nuevoNodo = new Nodo(valorBase);
		if (nodoRaiz == null && nuevoNodo.getNivel() == 0) {
			nodoRaiz = nuevoNodo;
			Nodo nodoDerecho = new Nodo(baseNodoDe);
			Nodo nodoIzquierdo = new Nodo(baseNodoIz);
			nodoRaiz.setNodoizquierdo(nodoIzquierdo);
			nodoRaiz.setNododerecho(nodoDerecho);
			nodoRaiz.setNodoPadre(null);
			nodoAnterior = nodoRaiz;
		} else {

			// System.out.println("Nodo Padre: " + nodoAnterior);
			if (valorBase == nodoAnterior.getNodoizquierdo().getValor()) {
				nodoAnterior.getNodoizquierdo().setNodoizquierdo(new Nodo(baseNodoIz));
				nodoAnterior.getNodoizquierdo().setNododerecho(new Nodo(baseNodoDe));
				nodoAnterior.getNodoizquierdo().setNivel(nivelBase);
				nodoAnterior.getNodoizquierdo().setNodoPadre(new Nodo(padrebase));

			} else if (valorBase == nodoAnterior.getNododerecho().getValor()) {
				nodoAnterior.getNododerecho().setNodoizquierdo(new Nodo(baseNodoIz));
				nodoAnterior.getNododerecho().setNododerecho(new Nodo(baseNodoDe));
				nodoAnterior.getNododerecho().setNivel(nivelBase);
				nodoAnterior.getNododerecho().setNodoPadre(new Nodo(padrebase));

			} else {
				Nodo nodoEncontrado = consultarNodo(valorBase, nodoRaiz);
				if (nodoEncontrado != null) {
					if (baseNodoIz != 0) {
						nodoEncontrado.setNodoizquierdo(new Nodo(baseNodoIz));
					}
					if (baseNodoDe != 0) {
						nodoEncontrado.setNododerecho(new Nodo(baseNodoDe));
					}
					nodoEncontrado.setNodoPadre(new Nodo(padrebase));

				}
			}
		}
	}

	/* (non-Javadoc)
	 * Permite consultar un nodo hijo
	 * @see com.test.consultaancestro.service.Arbol#consultarNodo(int, com.test.consultaancestro.pojo.Nodo)
	 */
	@Override
	public Nodo consultarNodo(int valorHijo, Nodo nodoPadreBusqueda) {
		Nodo nodoEncontrado = null;
		if (nodoPadreBusqueda != null) {
			int valorNodoDerechoBusqueda = nodoPadreBusqueda.getNododerecho() != null ? nodoPadreBusqueda.getNododerecho().getValor() : 0;
			int valorNodoIzquierdaBusqueda = nodoPadreBusqueda.getNodoizquierdo() != null ? nodoPadreBusqueda.getNodoizquierdo().getValor() : 0;

			if (valorHijo == valorNodoDerechoBusqueda) {
				return nodoEncontrado = nodoPadreBusqueda.getNododerecho();
			} else if (valorHijo == valorNodoIzquierdaBusqueda) {
				return nodoEncontrado = nodoPadreBusqueda.getNodoizquierdo();
			} else {
				nodoEncontrado = consultarNodo(valorHijo, nodoPadreBusqueda.getNodoizquierdo());
				if (nodoEncontrado != null)
					return nodoEncontrado;
				nodoEncontrado = consultarNodo(valorHijo, nodoPadreBusqueda.getNododerecho());
				if (nodoEncontrado != null)
					return nodoEncontrado;
			}
		}
		return nodoEncontrado;
	}

	/* (non-Javadoc)
	 * permite buscar el padre comun de varios nodos hijos
	 * @see com.test.consultaancestro.service.Arbol#buscarPadreComun(java.lang.Integer[])
	 */
	public Integer buscarPadreComun(Integer[] hijos) {
		int padreComun = 0;
		List listaFull = new ArrayList();
		int numeroListasEncontradas = 0;
		for (int i = 0; i < hijos.length; i++) {
			List listaPadres = new ArrayList();
			buscarPadres(hijos[i], listaPadres);
			if (listaPadres.size() > 0) {
				numeroListasEncontradas++;
			}
			for (Object nodo : listaPadres) {
				listaFull.add(nodo);
			}
		}
		if(listaFull == null || listaFull.isEmpty()){
			return -1;
		}
		for (Object nodo : listaFull) {
			if (Collections.frequency(listaFull, nodo) == numeroListasEncontradas) {
				return Integer.parseInt(nodo.toString());
			}
		}
		return padreComun;
	}
	
	/**
	 * Buscar padres.
	 *
	 *Busca los padres de un nodo 
	 * @param valorNodo the valor nodo
	 * @param listaPadres the lista padres
	 * @return the list
	 */
	public synchronized List buscarPadres(int valorNodo, List listaPadres) {
		Nodo nodoHijo = consultarNodo(valorNodo, nodoRaiz);
		if (nodoHijo != null && nodoHijo.getNodoPadre().getValor() != 0) {
			int nodoPadre = nodoHijo.getNodoPadre().getValor();
			listaPadres.add(nodoPadre);
			buscarPadres(nodoPadre, listaPadres);
		}
		return listaPadres;
	}
	
	/* (non-Javadoc)
	 *Permite consultar un numero de nodos en el arbol y retornar el ancestro comun 
	 * @see com.test.consultaancestro.service.Arbol#consultarArbol(java.lang.Integer[])
	 */
	@Override
	public Integer consultarArbol(Integer[] nodos) {
		if(nodoRaiz == null){
			this.cargarArbol();
		}
		Integer valor = buscarPadreComun(nodos);
		return valor;
	}
}