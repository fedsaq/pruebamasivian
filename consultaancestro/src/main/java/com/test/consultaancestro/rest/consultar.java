package com.test.consultaancestro.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.consultaancestro.service.Arbol;
import com.test.consultaancestro.util.LogConfiguration;
import com.test.consultaancestro.vo.Respuesta;

// TODO: Auto-generated Javadoc
/**
 * The Class consultar.
 */
@RestController
@RequestMapping("/rest")
public class consultar {

	/** The arbol. */
	@Autowired
	@Qualifier("arbolBinario")
	Arbol arbol;

	@Autowired
	@Qualifier("logConfiguration")
	LogConfiguration logConfiguration;
	
	/**
	 * Consultar ancestro comun.
	 *Permite consultar el ancestro comun de la aplicacion entre los parametros de entrada que se le ingresen 
	 *soporta hasta 5 para esta version
	 *
	 * @param nodo1 the nodo 1
	 * @param nodo2 the nodo 2
	 * @param nodo3 the nodo 3
	 * @param nodo4 the nodo 4
	 * @param nodo5 the nodo 5
	 * @return the response entity
	 */
	@RequestMapping(value = { ("/ancestrocomun/{nodo1}"), ("/ancestrocomun/{nodo1}/{nodo2}"), ("/ancestrocomun/{nodo1}/{nodo2}/{nodo3}"), ("/ancestrocomun/{nodo1}/{nodo2}/{nodo3}/{nodo4}"), ("/ancestrocomun/{nodo1}/{nodo2}/{nodo3}/{nodo4}/{nodo5}") }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Respuesta> consultarAncestroComun(@PathVariable Optional<Integer> nodo1, @PathVariable Optional<Integer> nodo2, @PathVariable Optional<Integer> nodo3, @PathVariable Optional<Integer> nodo4, @PathVariable Optional<Integer> nodo5) {
		logConfiguration.initLoggerAll();
		String idTransaccion = java.util.UUID.randomUUID().toString();
		logConfiguration.getLogger().info("consultarAncestroComun inicializada [Id TRansaccion "+idTransaccion+" ]");
		Integer iNodo1 = nodo1 != null ? nodo1.isPresent() ? new Integer(nodo1.get()) : null : null;
		Integer iNodo2 = nodo2 != null ? nodo2.isPresent() ? new Integer(nodo2.get()) : null : null;
		Integer iNodo3 = nodo3 != null ? nodo3.isPresent() ? new Integer(nodo3.get()) : null : null;
		Integer iNodo4 = nodo4 != null ? nodo4.isPresent() ? new Integer(nodo4.get()) : null : null;
		Integer iNodo5 = nodo5 != null ? nodo5.isPresent() ? new Integer(nodo5.get()) : null : null;

		Integer[] filtros = { iNodo1, iNodo2, iNodo3, iNodo4, iNodo5 };
		List<Integer> list = new ArrayList<Integer>(Arrays.asList(filtros));
		list.removeAll(Collections.singleton(null));
		Integer[] arregloEntrada = list.stream().toArray(Integer[]::new);
		StringBuilder mensaje = new StringBuilder();
		for (Integer valorMsj : arregloEntrada) {
			mensaje.append(valorMsj+" ");
		}
		logConfiguration.getLogger().info("Mensaje solicitud: "+ mensaje.toString()+" [Id TRansaccion "+idTransaccion+" ]");
		Integer consultarArbol = arbol.consultarArbol(arregloEntrada);
		logConfiguration.getLogger().info("Mensaje respuesta: "+ consultarArbol+" [Id TRansaccion "+idTransaccion+" ]");
		Respuesta respuesta = new Respuesta();
		respuesta.setAncestroComun(consultarArbol);
		logConfiguration.getLogger().info("consultarAncestroComun finalizada [Id TRansaccion "+idTransaccion+" ]");
		return new ResponseEntity<Respuesta>(respuesta, HttpStatus.OK);
	}

}
