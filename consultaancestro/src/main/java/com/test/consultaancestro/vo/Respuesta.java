package com.test.consultaancestro.vo;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * The Class Respuesta.
 */
public class Respuesta implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The ancestro comun. */
	private Integer ancestroComun;

	/**
	 * Instantiates a new respuesta.
	 */
	public Respuesta() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the ancestro comun.
	 *
	 * @return the ancestro comun
	 */
	public Integer getAncestroComun() {
		return ancestroComun;
	}

	/**
	 * Sets the ancestro comun.
	 *
	 * @param ancestroComun the new ancestro comun
	 */
	public void setAncestroComun(Integer ancestroComun) {
		this.ancestroComun = ancestroComun;
	}


}
