package com.test.consultaancestro.pojo;


//TODO: Auto-generated Javadoc
/**
* The Class Nodo.
*/
public class Nodo {

	/** The nodo padre. */
	Nodo nodoizquierdo, nododerecho,nodoPadre;
	
	/** The valor. */
	int valor;
	
	/** The nivel. */
	int nivel;

	/**
	 * Instantiates a new nodo.
	 *
	 * @param valor the valor
	 */
	public Nodo(int valor) {
		this.valor = valor;
		this.nodoizquierdo = null;
		this.nododerecho = null;
		this.nivel = 0;
	}

	/**
	 * Instantiates a new nodo.
	 *
	 * @param valor the valor
	 * @param nivel the nivel
	 * @param nodoIzquierdo the nodo izquierdo
	 * @param nodoDerecho the nodo derecho
	 */
	public Nodo(int valor, int nivel,Nodo nodoIzquierdo,Nodo nodoDerecho) {
		this.valor = valor;
		this.nodoizquierdo = nodoizquierdo;
		this.nododerecho = nododerecho;
		this.nivel = 0;
	}
	
	/**
	 * Gets the nodoizquierdo.
	 *
	 * @return the nodoizquierdo
	 */
	public Nodo getNodoizquierdo() {
		return nodoizquierdo;
	}



	/**
	 * Sets the nodoizquierdo.
	 *
	 * @param nodoizquierdo the new nodoizquierdo
	 */
	public void setNodoizquierdo(Nodo nodoizquierdo) {
		this.nodoizquierdo = nodoizquierdo;
	}



	/**
	 * Gets the nododerecho.
	 *
	 * @return the nododerecho
	 */
	public Nodo getNododerecho() {
		return nododerecho;
	}



	/**
	 * Sets the nododerecho.
	 *
	 * @param nododerecho the new nododerecho
	 */
	public void setNododerecho(Nodo nododerecho) {
		this.nododerecho = nododerecho;
	}



	/**
	 * Gets the nodo padre.
	 *
	 * @return the nodo padre
	 */
	public Nodo getNodoPadre() {
		return nodoPadre;
	}



	/**
	 * Sets the nodo padre.
	 *
	 * @param nodoPadre the new nodo padre
	 */
	public void setNodoPadre(Nodo nodoPadre) {
		this.nodoPadre = nodoPadre;
	}



	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	public int getValor() {
		return valor;
	}



	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(int valor) {
		this.valor = valor;
	}



	/**
	 * Gets the nivel.
	 *
	 * @return the nivel
	 */
	public int getNivel() {
		return nivel;
	}



	/**
	 * Sets the nivel.
	 *
	 * @param nivel the new nivel
	 */
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return " Valor nodo: "+valor +
				" Nodo izquierdo: "+ (nodoizquierdo!= null ? nodoizquierdo.getValor():" No Tiene asignado. ")+
				" Nodo Derecho: "+(nododerecho!= null ? nododerecho.getValor():" No Tiene asignado. ");
	}	
	
}