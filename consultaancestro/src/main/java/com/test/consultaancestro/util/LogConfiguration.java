package com.test.consultaancestro.util;

import java.io.File;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class LogConfiguration.
 * Cintiene toda la configuracion del log de la aplicacion.
 * 
 */
@Component("logConfiguration")
public class LogConfiguration {

	/** The logger. */
	public Logger logger;

	/**
	 * Default constructor.
	 */
	public LogConfiguration() {
		
	}

	/**
	 * Inits the logger all.
	 * Genera la instancia del log de la aplicacion.
	 */
	public void initLoggerAll() {
		Logger.getRootLogger().getLoggerRepository().resetConfiguration();

		String conversionPattern = EnumLog.PATRONCONVERSION.valor();
		Logger rootLogger = Logger.getRootLogger();
		PatternLayout layout = new PatternLayout();
		layout.setConversionPattern(conversionPattern);

		ConsoleAppender ca = new ConsoleAppender(layout);
		ca.activateOptions();
		ca.setThreshold(Level.ALL);

		rootLogger.addAppender(ca);

		RollingFileAppender rollingAppenderInfo = new RollingFileAppender();
		rollingAppenderInfo.setName(EnumLog.NOMBREAPPENDER.valor());
		rollingAppenderInfo.setLayout(layout);
		rollingAppenderInfo.setAppend(true);

		rollingAppenderInfo.setMaxFileSize("10MB");
		rollingAppenderInfo.setMaxBackupIndex(10);
		rollingAppenderInfo.setThreshold(Level.INFO);

		if (SystemUtils.IS_OS_WINDOWS) {
			rollingAppenderInfo.setFile(EnumLog.RUTA_WINDOWS.valor());
		} else if (SystemUtils.IS_OS_LINUX) {
			rollingAppenderInfo.setFile(EnumLog.RUTA_LINUX.valor());
		} else {
			rollingAppenderInfo.setFile(EnumLog.RUTA_LINUX.valor());
		}

		rollingAppenderInfo.activateOptions();
		rootLogger.addAppender(rollingAppenderInfo);
		logger = Logger.getLogger("transactionlog");

	}

	/**
	 * Gets the logger.
	 *Permite consultar la instancia del log de la aplicacion
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

}
