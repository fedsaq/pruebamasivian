package com.test.consultaancestro.util;

import java.io.File;

// TODO: Auto-generated Javadoc
/**
 * The Enum EnumLog.
 */
public enum EnumLog {

	/** The ruta windows. */
	RUTA_WINDOWS("C:" + File.separator + "PRUEBALOGS" + File.separator + "logtest.log"),
	
	/** The ruta linux. */
	RUTA_LINUX(File.separator + "home" + File.separator + "logtest.log"),
	
	/** The patronconversion. */
	PATRONCONVERSION("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n"),
    
    /** The maxfilesize. */
    MAXFILESIZE("10MB"),
    
    /** The maxbackupindex. */
    MAXBACKUPINDEX("10"),
    
    /** The nombreappender. */
    NOMBREAPPENDER("transactionlog");

    /** The valor. */
    private String valor;

    /**
     * Instantiates a new enum log.
     *
     * @param valor the valor
     */
    EnumLog(String valor) {
        this.valor = valor;
    }

    /**
     * Valor.
     *
     * @return the string
     */
    public String valor() {
        return valor;
    }
	
	
}
